import React from 'react'
import Image from "next/image";
import bannerImage from "../../../public/ranibow-banner-1920x875.jpg"
// import headerBg from "../../../public/header-pattern-final.png"



function banner() {
  return (
    <div className="banner">
         <Image
            className="relative m-auto"
            src={bannerImage}
            alt=" Logo"
            priority
        />
    </div>
  )
}

export default banner