"use client"
import { useState, useRef, useLayoutEffect } from 'react';
import { Dialog } from '@headlessui/react'
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline'
import Link from 'next/link'
import Image from "next/image";
import Logo from "../../../public/logo.svg"
import whiteLogo from "../../../public/Rambowcakery-white.png"
// import headerBg from "../../../public/header-pattern-final.png"

const bgImg = {
    // backgroundColor: '#69C2B8',
    backgroundImage: 'url(./header-pattern-final.png)',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat'
}
const navigation = [


    { name: 'ABOUT', href: '#' },
    { name: 'CAKES', href: '#' },
    { name: 'SWEET BITES', href: '#' },
    { name: 'CONTACT', href: '#' },
]




const Navbar = () => {
    const stickyHeader = useRef<HTMLDivElement>(null);
    useLayoutEffect(() => {
        const mainHeader = document.getElementById('mainHeader');
        const fixedHeader = () => {
            if (stickyHeader.current && window.pageYOffset > stickyHeader.current.offsetTop) {
                mainHeader?.classList.add('fixedTop');
            } else {
                mainHeader?.classList.remove('fixedTop');
            }
        };
        window.addEventListener('scroll', fixedHeader);
        return () => {
            window.removeEventListener('scroll', fixedHeader);
        };
    }, []);

    const [mobileMenuOpen, setMobileMenuOpen] = useState(false);
    return (
        <>
            <header className="header p-2 bg-danger-light" style={bgImg}>
                <div className="main-logo">
                    <div className="svg">
                        <Image
                            className="relative m-auto"
                            src={Logo}
                            alt=" Logo"
                            width={336}
                            height={128}
                            priority
                        />
                    </div>
                </div>

            </header>
            <nav className="mainHeader  p-4 lg:px-6 bg-danger" aria-label="Global" id="mainHeader" ref={stickyHeader}>
                <div className="container mr-auto ml-auto flex items-center justify-between">
                <div className="flex lg:flex-1">
                    <Link href="#" className="-m-1.5 p-1.5">
                    
                    <Image
                            className="relative m-auto"
                            src={whiteLogo}
                            alt=" Logo"
                            width={80}
                            height={61}
                            priority
                        />
                    </Link>
                </div>
                <div className="flex lg:hidden">
                    <button
                        type="button"
                        className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
                        onClick={() => setMobileMenuOpen(true)}
                    >
                        <span className="sr-only">Open main menu</span>
                        <Bars3Icon className="h-6 w-6" aria-hidden="true" />
                    </button>
                </div>
                <div className="hidden lg:flex lg:gap-x-12">
                    {navigation.map((item) => (
                        <Link key={item.name} href={item.href} className="text-xl leading-6 text-white ">
                            {item.name}
                        </Link>
                    ))}
                </div>
                <div className="invisible flex flex-1 justify-end">
                    <Link href="#" className="text-sm font-semibold leading-6 text-gray-900">
                        Log in <span aria-hidden="true">&rarr;</span>
                    </Link>
                </div>
                </div>
            </nav>
            <Dialog as="div" className="lg:hidden" open={mobileMenuOpen} onClose={setMobileMenuOpen}>
                <div className="fixed inset-0 z-50" />
                <Dialog.Panel className="fixed inset-y-0 right-0 z-50 w-full overflow-y-auto bg-white px-6 py-6 sm:max-w-sm sm:ring-1 sm:ring-gray-900/10">
                    <div className="flex items-center justify-between">
                        <Link href="#" className="-m-1.5 p-1.5">
                            <span className="sr-only">Your Company</span>
                            <img
                                className="h-8 w-auto"
                                src="https://tailwindui.com/img/logos/mark.svg?color=indigo&shade=600"
                                alt=""
                            />
                        </Link>
                        <button
                            type="button"
                            className="-m-2.5 rounded-md p-2.5 text-gray-700"
                            onClick={() => setMobileMenuOpen(false)}
                        >
                            <span className="sr-only">Close menu</span>
                            <XMarkIcon className="h-6 w-6" aria-hidden="true" />
                        </button>
                    </div>
                    <div className="mt-6 flow-root">
                        <div className="-my-6 divide-y divide-gray-500/10">
                            <div className="space-y-2 py-6">
                                {navigation.map((item) => (
                                    <Link
                                        key={item.name}
                                        href={item.href}
                                        className="-mx-3 block rounded-lg px-3 py-2 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                    >
                                        {item.name}
                                    </Link>
                                ))}
                            </div>
                            <div className="py-6">
                                <Link
                                    href="#"
                                    className="-mx-3 block rounded-lg px-3 py-2.5 text-base font-semibold leading-7 text-gray-900 hover:bg-gray-50"
                                >
                                    Log in
                                </Link>
                            </div>
                        </div>
                    </div>
                </Dialog.Panel>
            </Dialog>
        </>
    )
}

export default Navbar
