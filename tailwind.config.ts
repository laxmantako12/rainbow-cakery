import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  darkMode: "class",
  theme: {
    // screens: {
    //   sm: '640px',
    //   md: '768px',
    //   lg: '1024px',
    //   xl: '1200px',
    //   '2xl': '1536px',
    // },
    fontSize: {
      xs: "9px",
      sm: "14px",
      base: "18px",
      md: "21px",
      lg: "24px",
      xl: "26px",
      "2xl": "32px",
      "3xl": "36px",
      "4xl": "42px",
    },
    fontFamily: {
      Sniglet: ['Sniglet', 'cursive'],
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      light: "#f7f7f7",
      dark: "#04060D",
      primary: {
        light: "#143AA220",
        DEFAULT: "#143AA2",
      },
      secondary: {
        light: "#3E8DE320",
        DEFAULT: "#3E8DE3",
      },
      danger: {
        light: "rgba(252, 97, 114, .1)",
        DEFAULT: "#fb4e72",
      },
      success: {
        light: "#2EC4B620",
        DEFAULT: "#2EC4B6",
      },
      warning: {
        light: "#F2AF2920",
        DEFAULT: "#F2AF29",
      },
      muted: {
        light: "#eeeeee",
        DEFAULT: "#14141515",
        dark: "#04060D60",
      },
      white: {
        light: "#fff",
        DEFAULT: "#fff",
        dark: "#fff",
      },
    },
    extend: {
      // backgroundImage: {
      //   "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
      //   "gradient-conic":
      //     "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      // },
      borderRadius: {
        none: "0",
        sm: "0.125rem",
        DEFAULT: "0.25rem",
        md: "0.375rem",
        lg: "0.5rem",
        full: "50%",
        large: "12px",
      },
      // padding: {
      //   "3xs": "0.125rem",
      //   "2xs": "0.25rem",
      //   xs: "0.5rem",
      //   sm: "0.75rem",
      //   md: "1rem",
      //   lg: "1.25rem",
      //   xl: "1.5rem",
      //   "2xl": "1.75rem",
      //   "3xl": "2rem",
      //   "4xl": "3rem",
      //   "auto": "auto",
      //   "0": "0",
      // },
      // margin: {
      //   "3xs": "0.125rem",
      //   "2xs": "0.25rem",
      //   xs: "0.5rem",
      //   sm: "0.75rem",
      //   md: "1rem",
      //   lg: "1.25rem",
      //   xl: "1.5rem",
      //   "2xl": "1.75rem",
      //   "3xl": "2rem",
      //   "4xl": "3rem",
      //   "auto": "auto",
      // },
      transition: {
        "ease-in-out": "all 0.3s ease-in-out",
      },
      keyframes: {
        "slide-up": {
          "0%": {
            transform: "translateY(100%)",
          },
          "100%": {
            transform: "translateY(0)",
          },
        },
        "slide-down": {
          "0%": {
            transform: "translateY(0)",
          },
          "100%": {
            transform: "translateY(100%)",
          },
        },
        "fade-in": {
          "0%": {
            opacity: "0",
          },
          "100%": {
            opacity: "1",
          },
        },
        vibrate: {
          "0%": {
            transform: "translateX(0)",
          },
          "25%": {
            transform: "translateX(-2px)",
          },
          "50%": {
            transform: "translateX(2px)",
          },
          "75%": {
            transform: "translateX(-2px)",
          },
          "100%": {
            transform: "translateX(2px)",
          },
        },
      },
      animation: {
        "slide-up": "slide-up 0.3s ease-in-out",
        "slide-down": "slide-down 0.3s ease-in-out",
        "fade-in": "fade-in 0.3s ease-in-out",
        vibrate: "vibrate 0.3s linear",
      },
    },
  },
  plugins: [],
};
export default config;
